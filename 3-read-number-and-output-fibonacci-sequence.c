#include <stdio.h>

int fibonacci(int n) {
  if (n < 2)
    return n;

  return fibonacci(n - 1) + fibonacci(n - 2);
}

int main(int argc, char* argv[])
{
  int n, i, sum;

  printf("Enter a positive number: ");
  scanf("%d", &n);

  if (n < 0) {
    printf("Invalid number!\n");
    return(1);
  }

  for (i = 0; i < n; i++) {
    if (i < 2) {
      printf("%d ", i);
    } else {
      sum = fibonacci(i);
      printf("%d ", sum);
    }
  }

  return 0;
}
