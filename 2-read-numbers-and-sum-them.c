#include <stdio.h>

int main(int argc, char* argv[])
{
  float a, b, c, d, e;

  printf("Write 5 numbers: ");
  scanf("%f %f %f %f %f", &a, &b, &c, &d, &e);

  float sum;
  sum = a + b + c + d + e;

  printf("Sum of all numbers is %f \n", sum);

  return 0;
}
