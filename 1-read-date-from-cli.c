#include <stdio.h>
#include <string.h>
#include <time.h>

int main (int argc, char* argv[])
{
  printf("Enter your birth date: ");

  char date[11];
  scanf("%10s", &date);

  char yearChar[5];
  strncpy(yearChar, date + 6, 4);

  yearChar[4] = 0;

  int year = atoi(yearChar);

  time_t rawtime;
  struct tm * timeinfo;

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  int now = timeinfo->tm_year + 1900;
  int age = now - year;

  printf("You are %d years old\n", age);
  printf("After 10 years you will be %d years old\n", age + 10);

  return 0;
}
